package br.com.mvweb.drove.controller;

import br.com.mvweb.drove.controller.dto.SimpleVehicleDTO;
import br.com.mvweb.drove.controller.dto.VehicleDTO;
import br.com.mvweb.drove.domain.entity.Vehicle;
import br.com.mvweb.drove.service.VehicleService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/vehicles")
public class VehicleController {

    private ModelMapper modelMapper;
    private VehicleService service;

    @Autowired
    public VehicleController(ModelMapper modelMapper, VehicleService service) {
        this.modelMapper = modelMapper;
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<SimpleVehicleDTO>> list() {
        final List<SimpleVehicleDTO> vehicleDTOS = service.findAll().stream()
                .map(vehicle -> modelMapper.map(vehicle, SimpleVehicleDTO.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok(vehicleDTOS);
    }

    @GetMapping("/{id}")
    public ResponseEntity<VehicleDTO> findById(@PathVariable Long id) {
        return service.findById(id)
                .map(vehicle -> ResponseEntity.ok(modelMapper.map(vehicle, VehicleDTO.class)))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<VehicleDTO> create(@RequestBody VehicleDTO vehicleDTO) {
        final Vehicle vehicle = modelMapper.map(vehicleDTO, Vehicle.class);
        service.save(vehicle);

        return ResponseEntity.ok(modelMapper.map(vehicle, VehicleDTO.class));
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Long id, @RequestBody VehicleDTO vehicleDTO) {
        final Optional<Vehicle> optionalVehicle = service.findById(id);

        optionalVehicle.ifPresent(vehicle -> {
            final Vehicle updatedVehicle = modelMapper.map(vehicleDTO, Vehicle.class);
            updatedVehicle.setId(vehicle.getId());

            service.save(updatedVehicle);
        });
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        final Optional<Vehicle> optionalVehicle = service.findById(id);
        optionalVehicle.ifPresent(vehicle -> service.delete(vehicle));
    }

}
