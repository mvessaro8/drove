package br.com.mvweb.drove.controller.dto;

import java.io.Serializable;

public class SimpleVehicleDTO implements Serializable {

    public Long id;
    public String model;
    public String brand;

}
