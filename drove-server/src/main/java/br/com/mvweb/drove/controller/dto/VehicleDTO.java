package br.com.mvweb.drove.controller.dto;

import br.com.mvweb.drove.domain.entity.Vehicle;

import java.io.Serializable;
import java.math.BigDecimal;

public class VehicleDTO implements Serializable {

    public Long id;
    public Vehicle.Type type;
    public String model;
    public String brand;
    public BigDecimal price;
    public Long mileage;

}
