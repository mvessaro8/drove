package br.com.mvweb.drove.domain.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Vehicle {

    public enum Type {
        CAR,
        MOTORCYCLE,
        BUS,
        TRUCK
    }

    @Id
    @GeneratedValue
    private Long id;

    @Enumerated(EnumType.STRING)
    private Type type;

    private String model;
    private String brand;
    private BigDecimal price;
    private Long mileage;

    public Vehicle() {
    }

    public Vehicle(Type type, String model, String brand, BigDecimal price, Long mileage) {
        this.type = type;
        this.model = model;
        this.brand = brand;
        this.price = price;
        this.mileage = mileage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getMileage() {
        return mileage;
    }

    public void setMileage(Long mileage) {
        this.mileage = mileage;
    }

}
