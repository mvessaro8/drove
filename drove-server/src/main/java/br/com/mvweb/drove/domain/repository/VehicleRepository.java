package br.com.mvweb.drove.domain.repository;

import br.com.mvweb.drove.domain.entity.Vehicle;
import org.springframework.data.repository.CrudRepository;

public interface VehicleRepository extends CrudRepository<Vehicle, Long> {

}
