package br.com.mvweb.drove.service;

import br.com.mvweb.drove.domain.entity.Vehicle;
import br.com.mvweb.drove.domain.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehicleService {

    private VehicleRepository repository;

    @Autowired
    public VehicleService(VehicleRepository vehicleRepository) {
        this.repository = vehicleRepository;
    }

    public List<Vehicle> findAll() {
        return (List<Vehicle>) repository.findAll();
    }

    public Optional<Vehicle> findById(Long id) {
        return repository.findById(id);
    }

    public void save(Vehicle vehicle) {
        repository.save(vehicle);
    }

    public void delete(Vehicle vehicle) {
        repository.delete(vehicle);
    }

}
