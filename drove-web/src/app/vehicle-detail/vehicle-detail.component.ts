import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Vehicle } from '../vehicle';
import { VehicleService } from '../vehicle.service';

@Component({
  selector: 'app-vehicle-detail',
  templateUrl: './vehicle-detail.component.html',
  styleUrls: ['./vehicle-detail.component.css']
})
export class VehicleDetailComponent implements OnInit {

  @Input() vehicle: Vehicle;

  types = ['CAR', 'MOTORCYCLE', 'BUS', 'TRUCK'];

  constructor(private route: ActivatedRoute,
              private vehicleService: VehicleService,
              private location: Location) {}

  ngOnInit(): void {
    this.getVehicle();
  }

  private getVehicle(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.vehicleService.findById(id)
      .subscribe(vehicle => this.vehicle = vehicle);
  }

  goBack(): void {
    this.location.back();
  }

  update(): void {
    this.vehicleService.update(this.vehicle)
      .subscribe(() => this.goBack());
  }
}
