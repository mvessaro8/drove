import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Vehicle } from './vehicle';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class VehicleService {

  private vehiclesUrl = 'http://localhost:4200/api/vehicles';

  constructor(private http: HttpClient) { }

  list(): Observable<Vehicle[]> {
    return this.http.get<Vehicle[]>(this.vehiclesUrl).pipe(
      catchError(this.handleError('list', []))
    );
  }

  findById(id: number): Observable<Vehicle> {
    const url = `${this.vehiclesUrl}/${id}`;
    return this.http.get<Vehicle>(url).pipe(
      catchError(this.handleError<Vehicle>(`getVehicle id=${id}`))
    );
  }

  save(vehicle: Vehicle): Observable<Vehicle> {
    return this.http.post<Vehicle>(this.vehiclesUrl, vehicle, httpOptions).pipe(
      tap((v: Vehicle) => console.log(`added vehicle w/ id=${v.id}`)),
      catchError(this.handleError<Vehicle>('addVehicle'))
    );
  }

  update(vehicle: Vehicle): Observable<any> {
    const url = `${this.vehiclesUrl}/${vehicle.id}`;
    return this.http.put(url, vehicle, httpOptions).pipe(
      catchError(this.handleError<any>(`updateVehicle`))
    );
  }

  delete(vehicle: Vehicle | number): Observable<Vehicle> {
    const id = typeof vehicle === 'number' ? vehicle : vehicle.id;
    const url = `${this.vehiclesUrl}/${id}`;

    return this.http.delete<Vehicle>(url, httpOptions).pipe(
      catchError(this.handleError<Vehicle>('deleteVehicle'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

}
