export class Vehicle {
  id: number;
  type: string;
  model: string;
  brand: string;
  price: number;
  mileage: number;
}
