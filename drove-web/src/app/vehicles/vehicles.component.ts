import { Component, OnInit } from '@angular/core';
import { Vehicle } from '../vehicle';
import { VehicleService } from '../vehicle.service';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {

  vehicles: Vehicle[];

  constructor(private vehicleService: VehicleService) { }

  ngOnInit(): void {
    this.getVehicles();
  }

  private getVehicles(): void {
    this.vehicleService.list()
      .subscribe(vehicles => this.vehicles = vehicles);
  }

  private add(model: string = ''): void {
    model = model.trim();
    if (!model) { return; }
    this.vehicleService.save({ model } as Vehicle)
      .subscribe(vehicle => this.vehicles.push(vehicle));
  }

  private delete(vehicle: Vehicle): void {
    this.vehicleService.delete(vehicle)
      .subscribe(vehicles => {
        this.vehicles = this.vehicles.filter(v => v !== vehicle);
      });
  }
}
